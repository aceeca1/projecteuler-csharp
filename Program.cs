using System;

class Program {
    public static void Main() {
        for (;;) {
            var s = Console.ReadLine();
            if (s == null || s.Length == 0) break;
            Type.GetType("P" + s).GetMethod("Main").Invoke(null, null);
        }
    }
}
