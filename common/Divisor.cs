using System.Collections.Generic;

class Divisor {
    public static IEnumerable<T> Of<T>(T n, IOps<T> ops) {
        var i = ops.One();
        for (; ops.Less(ops.Mul(i, i), n); i = ops.Inc(i)) 
            if (ops.IsZero(ops.Mod(n, i))) {
                yield return i;
                yield return ops.Div(n, i);
            }
        if (ops.Eq(ops.Mul(i, i), n)) yield return i;
    }

    public static T Gcd<T>(T a, T b, IOps<T> ops) {
        for (;;) {
            if (ops.IsZero(b)) return a;
            var c = ops.Mod(a, b);
            a = b;
            b = c;
        }
    }
}
