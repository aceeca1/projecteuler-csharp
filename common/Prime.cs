class Prime {
    int[] A;
    
    public Prime(int n) {
        A = new int[n + 1];
        int u = 0;
        for (int i = 2; i <= n; ++i) {
            int v = A[i];
            if (v == 0) u = A[u] = v = i;
            for (int w = 2; i * w <= n; w = A[w]) {
                A[i * w] = w;
                if (w >= v) break;
            }
        }
        A[u] = n + 1;
    }

    public bool Is(int n) => n >= 2 && A[n] > n;
    public int Next(int n) => A[n];
    public int N() => A.Length - 1;
}

class PrimeA {
    static Prime P = new Prime(1000000);
    
    static void Ensure(int n) {
        while (n > P.N()) P = new Prime(P.N() << 1);
    }

    public static bool Is(int n) {
        if (n < 2) return false;
        Ensure(n);
        return P.Is(n);
    }
}
