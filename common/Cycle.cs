using System;

abstract class Cycle<T> {
    protected abstract T Init();
    protected abstract T Next(T v);
    protected abstract bool Eq(T v1, T v2);
    
    public int Length() {
        int i1 = 0, i2 = 1;
        T t1 = Init(), t2 = Next(Init());
        while (!Eq(t1, t2)) {
            if (i2 > i1 + i1) { i1 = i2; t1 = t2; }
            ++i2; 
            t2 = Next(t2);
        }
        return i2 - i1;
    }
}
