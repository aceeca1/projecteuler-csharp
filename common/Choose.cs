class Choose {
    public static T Num<T>(int n, int k, IOps<T> ops) {
        var ret = ops.One();
        for (int i = 1; i <= k; ++i)
            ret = ops.Div(ops.Mul(ret, ops.From(n - i + 1)), ops.From(i));
        return ret;
    }
}
