using System.Collections.Generic;

class Factor {
    public struct Pk<T> {
        public T P;
        public int K;
    }
    
    public static IEnumerable<Pk<T>> Of<T>(T n, IOps<T> ops) {
        for (var p = ops.From(2); !ops.Less(n, ops.Mul(p, p)); p = ops.Inc(p)) {
            int k = 0;
            while (ops.IsZero(ops.Mod(n, p))) {
                n = ops.Div(n, p);
                ++k;
            }
            if (k != 0) yield return new Pk<T> { P = p, K = k };
        }
        if (!ops.IsOne(n)) yield return new Pk<T> { P = n, K = 1 };
    }
}
