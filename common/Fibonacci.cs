using System.Collections.Generic;

class Fibonacci {
    public static IEnumerable<T> From<T>(T a1, T a2, IOps<T> ops) {
        for (;;) {
            yield return a1;
            var a3 = ops.Add(a1, a2);
            a1 = a2;
            a2 = a3;
        }
    }
}
