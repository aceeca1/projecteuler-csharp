using System;

class P7 {
    public static void Main() {
        int n = 10000;
        var prime = new Prime((int)(n * Math.Log(n) * 1.5));
        int ans = 2;
        for (int i = 2; i <= 10001; ++i)
            ans = prime.Next(ans);
        Console.WriteLine(ans);
    }
}
