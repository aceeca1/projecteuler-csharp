using System;

class P5 {
    public static void Main() {
        int ans = 1;
        for (int i = 1; i <= 20; ++i)
            ans *= i / Divisor.Gcd(ans, i, new OpsInt());
        Console.WriteLine(ans);
    }
}
