using System;
using System.Linq;

class P22 {
    public static void Main() {
        string[] a;
        using (var file = new System.IO.StreamReader("data/input22.txt")) {
            var b = file.ReadLine().Split(',');
            a = new string[b.Length];
            for (int i = 0; i < b.Length; ++i)
                a[i] = b[i].Substring(1, b[i].Length - 2);
        }
        Array.Sort(a);
        int ans = 0;
        for (int i = 0; i < a.Length; ++i)
            ans += (i + 1) * a[i].Sum(k => k - 'A' + 1);
        Console.WriteLine(ans);
    }
}
