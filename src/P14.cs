﻿using System;

class P14 {
    const int N = 1000000;
    static long Next(long v) => (v & 1L) == 0L ? v >> 1 : (v + v) + (v + 1L);
    static int[] A = new int[N + 1];

    static int CollatzLen(long v) {
        if (v > N) return CollatzLen(Next(v)) + 1;
        if (A[v] != 0) return A[v];
        int ans = CollatzLen(Next(v)) + 1;
        A[v] = ans;
        return ans;
    }

    public static void Main() {
        A[1] = 1;
        int ans = 0;
        for (int i = 1; i <= N; ++i)
            if (CollatzLen(i) > A[ans]) ans = i;
        Console.WriteLine(ans);
    }
}
