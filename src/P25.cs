using System;
using System.Linq;
using System.Numerics;

class P25 {
    public static void Main() {
        Console.WriteLine(Fibonacci.From(
            BigInteger.One, BigInteger.One, new OpsBigInteger()
        ).TakeWhile(k => k.ToString().Length < 1000).Count() + 1);
    }
}
