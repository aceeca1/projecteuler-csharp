using System;
using System.Linq;

class P34 {
    public static void Main() {
        var factorial = new int[10];
        factorial[0] = 1;
        for (int i = 1; i < 10; ++i) factorial[i] = factorial[i - 1] * i;
        int answer = 0;
        for (int i = 10; i < factorial[9] * 7; ++i)
            if (i == i.ToString().Sum(k => factorial[k - '0'])) answer += i;
        Console.WriteLine(answer);
    }
}
