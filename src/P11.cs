using System;

class P11 {
    static int[] Dx = { 0, 1, 1, 1 };
    static int[] Dy = { 1, 0, 1, -1 };
    
    const int N = 20;
    static int[,] A = new int[20, 20];

    static int product(int x, int y, int d) {
        int ans = 1;
        for (int i = 0; i < 4; ++i) {
            if (!(0 <= x && x < N)) return 0;
            if (!(0 <= y && y < N)) return 0;
            ans *= A[x, y];
            x += Dx[d];
            y += Dy[d];
        }
        return ans;
    }
    
    public static void Main() {
        using (var file = new System.IO.StreamReader("data/input11.txt")) {
            for (int i = 0; i < 20; ++i) {
                var ai = file.ReadLine().Split();
                for (int j = 0; j < 20; ++j) A[i, j] = int.Parse(ai[j]);
            }
        }
        int ans = 0;
        for (int i = 0; i < N; ++i)
            for (int j = 0; j < N; ++j)
                for (int k = 0; k < Dx.Length; ++k) {
                    int ans1 = product(i, j, k);
                    if (ans1 > ans) ans = ans1;
                }
        Console.WriteLine(ans);
    }
}
