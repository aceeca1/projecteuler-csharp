using System;

class P31 {
    const int N = 200;

    public static void Main() {
        var a = new int[N + 1];
        a[0] = 1;
        foreach (var i in new int[] { 1, 2, 5, 10, 20, 50, 100, 200 })
            for (int j = i; j <= N; ++j) a[j] += a[j - i];
        Console.WriteLine(a[N]);
    }
}
