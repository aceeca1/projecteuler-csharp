using System;
using System.Collections.Generic;
using System.Linq;

class P33 {
    struct Fraction {
        public int Numerator, Denominator;
        public static Fraction operator*(Fraction Lhs, Fraction Rhs) {
            return new Fraction {
                Numerator = Lhs.Numerator * Rhs.Numerator,
                Denominator = Lhs.Denominator * Rhs.Denominator
            };
        }

        public Fraction Simplify() {
            var gcd = Divisor.Gcd(Numerator, Denominator, new OpsInt());
            Numerator /= gcd;
            Denominator /= gcd;
            return this;
        }
    }

    static IEnumerable<Fraction> AllSolutions() {
        for (int n1 = 1; n1 <= 9; ++n1) {
            var m0 = n1;
            for (int n0 = 0; n0 <= 9; ++n0) {
                var n = n1 * 10 + n0;
                for (int m1 = 1; m1 <= 9; ++m1) {
                    var m = m1 * 10 + m0;
                    if (m != n && m * n0 == n * m1)
                        yield return new Fraction {
                            Numerator = m,
                            Denominator = n
                        };
                }
            }
        }
    }

    public static void Main() {
        var answer = new Fraction {
            Numerator = 1,
            Denominator = 1
        };
        foreach (var i in AllSolutions()) answer *= i;
        Console.WriteLine(answer.Simplify().Denominator);
    }
}
