using System;
using System.Linq;
using System.Collections.Generic;

class P17 {
    class Pronounce {
        public List<string> Result = new List<string>();
    
        static string Direct(int n) {
            switch (n) {
                case 1: return "one";
                case 2: return "two";
                case 3: return "three";
                case 4: return "four";
                case 5: return "five";
                case 6: return "six";
                case 7: return "seven";
                case 8: return "eight";
                case 9: return "nine";
                case 10: return "ten";
                case 11: return "eleven";
                case 12: return "twelve";
                case 13: return "thirteen";
                case 14: return "fourteen";
                case 15: return "fifteen";
                case 16: return "sixteen";
                case 17: return "seventeen";
                case 18: return "eighteen";
                case 19: return "nineteen";
                case 20: return "twenty";
                case 30: return "thirty";
                case 40: return "forty";
                case 50: return "fifty";
                case 60: return "sixty";
                case 70: return "seventy";
                case 80: return "eighty";
                case 90: return "ninety";
                default: return null;
            }
        }
        
        void AppendXX(int n) {
            var p = Direct(n);
            if (p == null) {
                Result.Add(Direct(n / 10 * 10));
                Result.Add(Direct(n % 10));
            } else Result.Add(p);
        }
        
        void AppendX00(int n) {
            Result.Add(Direct(n));
            Result.Add("hundred");
        }
        
        public Pronounce(int n) {
            if (n == 1000) {
                Result.Add("one");
                Result.Add("thousand");
            } else if (n < 100) { 
                AppendXX(n);
            } else {
                AppendX00(n / 100);
                n %= 100;
                if (n == 0) return;
                Result.Add("and");
                AppendXX(n);
            }
        }
    }
    
    public static void Main() {
        int ans = 0;
        for (int i = 1; i <= 1000; ++i)
            ans += new Pronounce(i).Result.Sum(k => k.Length);
        Console.WriteLine(ans);
    }
}