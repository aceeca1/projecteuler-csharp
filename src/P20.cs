using System;
using System.Linq;
using System.Numerics;

class P20 {
    public static void Main() {
        BigInteger a = 1;
        for (int i = 2; i <= 100; ++i) a *= i;
        Console.WriteLine(a.ToString().Sum(k => k - '0'));
    }
}
