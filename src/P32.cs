using System;
using System.Collections.Generic;
using System.Linq;

class P32 {
    static bool HasOneToNine(params int[] a) {
        int s = 0;
        foreach (var i in a)
            foreach (var j in i.ToString())
                s |= 1 << (j - '0');
        return s == 0x3fe;
    }

    static IEnumerable<int> f(int n1, int n2, int n3) {
        for (int i1 = n1 / 10; i1 < n1; ++i1)
            for (int i2 = n2 / 10; i2 < n2; ++i2) {
                var i3 = i1 * i2;
                if (n3 <= i3) break;
                if (HasOneToNine(i1, i2, i3)) yield return i3;
            }
    }

    public static void Main() {
        var a = new HashSet<int>();
        a.UnionWith(f(100, 1000, 10000));
        a.UnionWith(f(10, 10000, 10000));
        Console.WriteLine(a.Sum());
    }
}
