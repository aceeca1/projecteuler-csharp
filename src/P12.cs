using System;
using System.Linq;

class P12 {
    public static void Main() {
        int n = 1;
        int m = 2;
        while (Divisor.Of(n, new OpsInt()).Count() <= 500) {
            n += m;
            ++m;
        }
        Console.WriteLine(n);
    }
}
