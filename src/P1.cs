using System;

class P1 {
    public static void Main() {
        int ans = 0;
        for (int i = 0; i < 1000; ++i)
            if (i % 3 == 0 || i % 5 == 0) ans += i;
        Console.WriteLine(ans);
    }
}
