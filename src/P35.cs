using System;
using System.Collections.Generic;

class P35 {
    const int N = 1000000;

    static string Rotate(string s, int n) {
        n %= s.Length;
        return s.Substring(n) + s.Substring(0, n);
    }

    public static void Main() {
        var prime = new Prime(N);
        var primeSet = new HashSet<string>();
        for (int i = 2; i < N; i = prime.Next(i)) primeSet.Add(i.ToString());
        foreach (var i in new int[] { 4, 2, 1 }) {
            var newSet = new HashSet<string>();
            foreach (var j in primeSet)
                if (primeSet.Contains(Rotate(j, i))) newSet.Add(j);
            primeSet = newSet;
        }
        Console.WriteLine(primeSet.Count);
    }
}
