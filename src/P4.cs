using System;

class P4 {
    static bool isPalindrome(int n) {
        var s = n.ToString();
        var a = s.ToCharArray();
        Array.Reverse(a);
        return s == new String(a);
    }
    
    public static void Main() {
        int ans = 0;
        for (int i1 = 100; i1 < 1000; ++i1)
            for (int i2 = 100; i2 < 1000; ++i2) {
                int n = i1 * i2;
                if (isPalindrome(n)) {
                    if (n > ans) ans = n;
                }
            }
        Console.WriteLine(ans);
    }
}
