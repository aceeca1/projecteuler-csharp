using System;
using System.Linq;
using System.Numerics;

class P16 {
    public static void Main() {
        var a = BigInteger.One << 1000;
        Console.WriteLine(a.ToString().Sum(c => c - '0'));
    }
}
