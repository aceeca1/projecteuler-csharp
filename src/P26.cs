using System;

class P26 {
    class ReciprocalCycle: Cycle<int> {
        int N;
        public ReciprocalCycle(int n) { N = n; }
        
        protected override int Init() => 1;
        protected override int Next(int v) => v * 10 % N;
        protected override bool Eq(int v1, int v2) => v1 == v2;
    }
    
    public static void Main() {
        int max = 0;
        int arg = -1;
        for (int i = 1; i <= 1000; ++i) {
            var len = new ReciprocalCycle(i).Length();
            if (len > max) {
                max = len;
                arg = i;
            }
        }
        Console.WriteLine(arg);
    }
}
