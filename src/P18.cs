using System;

class P18 {
    public static void Main() {
        int n = 15;
        var a = new int[n][];
        using (var file = new System.IO.StreamReader("data/input18.txt")) {
            for (int i = 0; i < n; ++i) {
                var ai = file.ReadLine().Split();
                a[i] = new int[ai.Length];
                for (int j = 0; j < ai.Length; ++j)
                    a[i][j] = int.Parse(ai[j]);
            }
        }
        var ans = a[n - 1];
        for (int i = n - 2; i >= 0; --i)
            for (int j = 0; j <= i; ++j)
                ans[j] = Math.Max(ans[j], ans[j + 1]) + a[i][j];
        Console.WriteLine(ans[0]);
    }
}
