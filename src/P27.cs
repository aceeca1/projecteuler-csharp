using System;

class P27 {
    public static void Main() {
        int max = 0;
        int arg = 0;
        for (int a = -999; a <= 999; ++a)
            for (int b = -1000; b <= 1000; ++b) {
                int n = 0;
                while (PrimeA.Is((n + a) * n + b)) ++n;
                if (n > max) {
                    max = n;
                    arg = a * b;
                }
            }
        Console.WriteLine(arg);
    }
}
