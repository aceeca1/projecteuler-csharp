using System;

class P8 {
    class ProductZ {
        long Product = 1L;
        int Zero = 0;
        
        public void Add(char c) {
            if (c == '0') ++Zero;
            else Product *= c - '0';
        }
        
        public void Del(char c) {
            if (c == '0') --Zero;
            else Product /= c - '0';
        }
        
        public long Prod => Zero == 0 ? Product : 0L;
    }
    
    public static void Main() {
        var sb = new System.Text.StringBuilder();
        using (var file = new System.IO.StreamReader("data/input8.txt")) {
            for (int i = 0; i < 20; ++i) sb.Append(file.ReadLine());
        }
        var pz = new ProductZ();
        long ans = 0L;
        for (int i = 0; i < 13; ++i) pz.Add(sb[i]);
        for (int i = 13; ; ++i) {
            if (pz.Prod > ans) ans = pz.Prod;
            if (i == sb.Length) break;
            pz.Add(sb[i]);
            pz.Del(sb[i - 13]);
        }
        Console.WriteLine(ans);
    }
}
