using System;
using System.Linq;

class P21 {
    static bool isAmicable(int n) {
        int m = Divisor.Of(n, new OpsInt()).Sum() - n;
        if (m == n) return false;
        int u = Divisor.Of(m, new OpsInt()).Sum() - m;
        return u == n;
    }
    
    public static void Main() {
        int ans = 0;
        for (int i = 1; i <= 10000; ++i)
            if (isAmicable(i)) ans += i;
        Console.WriteLine(ans);
    }
}
