using System;
using System.Numerics;

class P13 {
    public static void Main() {
        var sum = BigInteger.Zero;
        using (var file = new System.IO.StreamReader("data/input13.txt")) {
            for (int i = 0; i < 100; ++i)
                sum += BigInteger.Parse(file.ReadLine());
        }
        Console.WriteLine(sum.ToString().Substring(0, 10));
    }
}
