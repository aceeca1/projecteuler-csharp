using System;

class P2 {
    public static void Main() {
        int ans = 0;
        foreach (var i in Fibonacci.From(1, 1, new OpsInt())) {
            if (i > 4000000) break;
            if ((i & 1) == 0) ans += i;
        }
        Console.WriteLine(ans);
    }
}
