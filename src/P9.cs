using System;

class P9 {
    public static void Main() {
        int ans = 0;
        for (int a = 1; a <= 1000; ++a) {
            int s1 = 1000 - a;
            int s2 = a * a;
            if (s2 % s1 != 0) continue;
            int b2 = s1 - s2 / s1;
            if (b2 < 0 || (b2 & 1) != 0) continue;
            int b = b2 >> 1;
            int c = s1 - b;
            ans = a * b * c;
            break;
        }
        Console.WriteLine(ans);
    }
}
