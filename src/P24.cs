using System;
using System.Linq;
using System.Collections.Generic;

class P24 {
    public static void Main() {
        var a = new int[10];
        int n = 1000000 - 1;
        for (int i = 1; i <= 10; ++i) {
            a[10 - i] = n % i;
            n /= i;
        }
        var available = new List<int>(Enumerable.Range(0, 10));
        for (int i = 0; i < 10; ++i) {
            Console.Write(available[a[i]]);
            available.RemoveAt(a[i]);
        }
        Console.WriteLine();
    }
}
