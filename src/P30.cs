using System;
using System.Linq;

class P30 {
    public static void Main() {
        var pow5 = new int[10];
        for (int i = 0; i < 10; ++i) pow5[i] = i * i * (i * i) * i;
        int ans = 0;
        for (int i = 6 * pow5[9]; i >= 2; --i) 
            if (i.ToString().Sum(k => pow5[k - '0']) == i) ans += i;
        Console.WriteLine(ans);
    }
}
