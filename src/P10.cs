using System;

class P10 {
    public static void Main() {
        int n = 2000000;
        var prime = new Prime(n);
        long ans = 0L;
        for (int i = 2; i < n; i = prime.Next(i)) ans += i;
        Console.WriteLine(ans);
    }
}
