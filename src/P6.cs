using System;

class P6 {
    public static void Main() {
        int n = 100;
        int a1 = n * (n + 1) >> 1;
        int a2 = n * (n + 1) * (n + n + 1) / 6;
        Console.WriteLine(a1 * a1 - a2);
    }
}
