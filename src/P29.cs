using System;
using System.Numerics;
using System.Collections.Generic;

class P29 {
    public static void Main() {
        var c = new HashSet<BigInteger>();
        for (int i = 2; i <= 100; ++i) {
            BigInteger k = i;
            for (int j = 2; j <= 100; ++j) {
                k *= i;
                c.Add(k);
            }
        }
        Console.WriteLine(c.Count);
    }
}
