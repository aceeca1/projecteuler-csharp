using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

class P23 {
    public static void Main() {
        int n = 28123;
        var ab = new List<int>();
        for (int i = 1; i <= n; ++i)
            if (Divisor.Of(i, new OpsInt()).Sum() - i > i) ab.Add(i);
        var a = new BitArray(n + 1);
        for (int i = 0; i < ab.Count; ++i)
            for (int j = i; j < ab.Count; ++j) {
                int s = ab[i] + ab[j];
                if (s > n) break;
                a.Set(s, true);
            }
        int ans = 0;
        for (int i = 0; i <= n; ++i)
            if (!a[i]) ans += i;
        Console.WriteLine(ans);
    }
}
