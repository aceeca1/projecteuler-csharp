using System;

class P19 {
    public static void Main() {
        int ans = 0;
        for (int i = 1901; i <= 2000; ++i)
            for (int j = 1; j <= 12; ++j)
                if (new DateTime(i, j, 1).DayOfWeek == DayOfWeek.Sunday) ++ans;
        Console.WriteLine(ans);
    }
}
